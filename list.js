var List = function(dest) {
  let getElement = selector => document.querySelector(selector);
  let getItems = () => JSON.parse(localStorage.getItem("Items"));
  let saveItems = params =>
    localStorage.setItem("Items", JSON.stringify(params));
  let items = [];
  let initModule = () => {
    if (localStorage.getItem("Items")) {
      items = getItems();
    }
    dest.innerHTML = createStructure();
    let list = getElement(".list");
    items.length > 0 ? (list.innerHTML = renderItems(items)) : null;

    let buttonAdd = getElement(".btn-add");
    let buttonClear = getElement(".btn-clear");
    buttonAdd.addEventListener("click", addItem);
    buttonClear.addEventListener("click", clearList);
    list.addEventListener("click",handleItemAction)
    Delete = document.querySelectorAll(".btn-delete");
    Hold = document.querySelectorAll(".btn-hold");

  };

  let createStructure = () => `
                              <div class="todo">
                                  <h1 class="todo__heading">Heading</h1>
                                  <div class="todo__add">
                                      <input type="text" class="input-text">
                                      <button class="btn btn-add">Add</button>
                                  </div>
                                  <ul class="list"></ul>
                                  <button class="btn btn-clear">Clear all!</button>
                              </div>`;

  let itemStructure = (id, text,hold) => `<li class="item ${hold?"checked":""}" key="${id}">
                                  <div class="item__text">${text}</div>
                                  <div class="btn-group">
                                      <button class="btn btn-hold" key="${id}">Hold</button>
                                      <button class="btn btn-delete" key="${id}">X</button>
                                  </div>
                                </li>`;

  let addItem = () => {
    let input = getElement(".input-text");
    let list = getElement(".list");
    if (input.value) {
      list.innerHTML = "";
      items.push({
        text: input.value,
        id: new Date().getTime(),
        hold: false
      });
      input.value = "";
      saveItems(items);
      list.innerHTML = renderItems(items);
      Delete = document.querySelectorAll(".btn-delete");
      Hold = document.querySelectorAll(".btn-hold");

    }
  };

  let handleItemAction=(event)=>{
    let target = event.target;
    while(target != event.currentTarget){
      if(target.classList.contains("item")){
        if(event.target.classList.contains("btn-delete")){
            deleteItem(target)
            return;
          }
          if(event.target.classList.contains("btn-hold"))
          {
            holdItem(target)
            return;
          }
      }
      target=target.parentNode;
    }
  }

  let deleteItem = item => {
    let input = getElement(".input-text");
    let buttonAdd = getElement(".btn-add");
    let key=item.getAttribute("key");
    Delete = document.querySelectorAll(".btn-delete");
    input.disabled = false;
    buttonAdd.disabled = false;
    input.value="";

    item.remove();

    items = items.filter(i => i.id != key);
    saveItems(items);
  };

  let holdItem =(item)=>{
    key = item.getAttribute("key");
    items = items.map(item=>{
      if(item.id==key){
        item.hold==true?item.hold=false:item.hold=true;
      }
      return item;
    })
    saveItems(items);
    item.classList.toggle('checked')
  }

  let renderItems = items => {
    let input = getElement(".input-text");
    let buttonAdd = getElement(".btn-add");
    if (items.length > 3) {
      
      input.disabled = true;
      buttonAdd.disabled = true;
      input.value="4 items is a maximum"
    }
    let result = "";
    for (const key in items) {
      result += itemStructure(items[key].id, items[key].text, items[key].hold);
    }
    return result;
  };

  function clearList() {
    let input = getElement(".input-text");
    let buttonAdd = getElement(".btn-add");
    let list = getElement(".list");
    input.disabled = false;
    input.value="";
    buttonAdd.disabled = false;
    items = items.filter(item=>item.hold==true);
    saveItems(items);
    list.innerHTML = renderItems(items);
  }

  return {
    init: initModule
  };
};
